import { Injectable } from '@angular/core';
import { Kana } from '../models/kana.model';
import { Hiragana } from '../data/hiragana';
import { Katakana } from '../data/katakana';
import { OptionsService } from './options.service';
//import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class KanaService {
  kanas: Kana[] = [];
  constructor(private _optionsService: OptionsService) {
    this.cargarStorage();
  }

  rellenaKana(array:any) {
    this.kanas = [];
    array.forEach( (el: any) => {
      const kana = new Kana(el.kana, el.fonema, el.type, el.helpDesc, el.active);
      this.kanas.push(kana);
    })
  }

  guardarStorage() {
    if(this._optionsService.cargarStorageOptions().isHiragana) {
      localStorage.setItem('hiragana', JSON.stringify(this.kanas))
    } else {
      localStorage.setItem('katakana', JSON.stringify(this.kanas))
    }
  }

  cargarDefault() {
    this.rellenaKana(this._optionsService.cargarStorageOptions().isHiragana ? Hiragana : Katakana)
  }

  /*cambiarTipo(isHiragana) {
    this.rellenaKana(isHiragana ? Hiragana : Katakana)
  }*/

  deleteStorage() {
    localStorage.removeItem('hiragana')
    localStorage.removeItem('katakana')
  }

  cargarStorage() {
    const lista: any = this._optionsService.cargarStorageOptions().isHiragana ? localStorage.getItem('hiragana') : localStorage.getItem('katakana')
    if(lista) {
      this.rellenaKana(JSON.parse(lista))
    } else {
      this.cargarDefault()
    }
  }
}
