import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class OptionsService {

  constructor() { }

  guardarStorage(options) {
    localStorage.setItem('options', JSON.stringify(options))
    //this.storage.set('kana', this.kanas);
  }

  cargarStorageOptions() {
    const options = localStorage.getItem('options')
    if(options) {
      return JSON.parse(options)
    }
    return this.cargarDefaultOptions();
  }

  cargarDefaultOptions() {
    return {
      numCards: 30,
      isHiragana: true
    }
  }
  deleteStorageOptions() {
    localStorage.removeItem('options')
  }
}
