export class Kana {
  kana: string;
  fonema: string;
  type: string;
  helpDesc: string;
  active: boolean;
  swipe?: boolean;
  constructor(kana: string, fonema:string, type: string, helpDesc:string, active: boolean) {
    this.kana = kana;
    this.fonema = fonema;
    this.helpDesc = helpDesc;
    this.type = type;
    this.active = active;
  }

  changeActive() {
    this.active = !this.active;
  }
}