import { Kana } from './kana.model';

export class TypeKana {
  //active: boolean;
  title: string;
  kanas: Kana[];
  constructor(title: string) {
    this.title = title;
    this.kanas = [];
  }
}