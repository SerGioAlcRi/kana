import { Component, OnInit, Input } from '@angular/core';
import { Kana } from 'src/app/models/kana.model';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
})
export class CardComponent implements OnInit {
  @Input() kana: Kana;
  //@Input() swipe: boolean;
  constructor() { 
    //this.swipe = false;
  }

  ngOnInit() {}

}
