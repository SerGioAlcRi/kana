import { Component } from '@angular/core';
import { KanaService } from 'src/app/services/kana.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(public kanaService:KanaService) {}

}
