import { Component, OnInit } from '@angular/core';
import { KanaService } from 'src/app/services/kana.service';
import { Kana } from 'src/app/models/kana.model';

@Component({
  selector: 'app-list',
  templateUrl: 'list.page.html',
  styleUrls: ['list.page.scss']
})
export class ListPage implements OnInit {
  kanas: Kana[] = [];
  constructor(public _kanaService: KanaService) {
    this.kanas = _kanaService.kanas;
  }

  ngOnInit() {
  }
  // add back when alpha.4 is out
  // navigate(item) {
  //   this.router.navigate(['/list', JSON.stringify(item)]);
  // }
}
