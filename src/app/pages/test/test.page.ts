import { Component, OnInit } from '@angular/core';
import { KanaService } from 'src/app/services/kana.service';
import { Kana } from 'src/app/models/kana.model';
import { OptionsService } from 'src/app/services/options.service';

@Component({
  selector: 'app-test',
  templateUrl: 'test.page.html',
  styleUrls: ['test.page.scss']
})
export class TestPage implements OnInit {
  kanas: Kana[] = [];
  slideOpts: any = {
    initialSlide: 1,
    speed: 400
  };
  constructor(public _kanaService: KanaService, public optionsService: OptionsService) {
    this.getKanasToTest();
  }

  ngOnInit() {
    console.log('OnInit');
    this.getKanasToTest();
  }

  refresh() {
    this.getKanasToTest();
  }

  getKanasToTest() {
    const numCards = this.optionsService.cargarStorageOptions().numCards;
    this.kanas = this._kanaService.kanas.filter((el: any) => el.active).map((el) => {
      el.swipe = Math.random() >= 0.5 ? true : false;
      return el;
    }).sort(() => Math.random() - 0.5 ).slice(0, Number(numCards)); 
  }
  // add back when alpha.4 is out
  // navigate(item) {
  //   this.router.navigate(['/list', JSON.stringify(item)]);
  // }
}
