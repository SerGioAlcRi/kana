import { Component } from '@angular/core';
import { KanaService } from 'src/app/services/kana.service';
import { Kana } from 'src/app/models/kana.model';
import { OptionsService } from 'src/app/services/options.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-options',
  templateUrl: 'options.page.html',
  styleUrls: ['options.page.scss']
})
export class OptionsPage {
  numCards: string;
  isHiragana: boolean;
  kanas: Kana[] = [];
  constructor(public _kanaService: KanaService, public _optionsService: OptionsService, public alertController: AlertController) {
    this.numCards = _optionsService.cargarStorageOptions().numCards;
    this.isHiragana = _optionsService.cargarStorageOptions().isHiragana;
    this.kanas = _kanaService.kanas;
  }

  async numCardChanged(data:any) {
    if(data > 0) {
      this._optionsService.guardarStorage({numCards: data, isHiragana: this.isHiragana})
    } else {
      console.log('Error')
      
      const alert = await this.alertController.create({
        header: 'Alerta',
        subHeader: 'Problema con numero de tarjetas',
        message: 'Debe ser un numero superior a 0.',
        buttons: ['Aceptar']
      });
  
      alert.present();
    }
  }

  changeType() {
    this.isHiragana = !this.isHiragana;
    this._optionsService.guardarStorage({numCards: this._optionsService.cargarStorageOptions().numCards, isHiragana: this.isHiragana})
    //this._kanaService.guardarStorage();
    this._kanaService.cargarStorage();    
    this.kanas = this._kanaService.kanas;
  }

  default() {
    this._kanaService.deleteStorage();
    this._optionsService.deleteStorageOptions();
    this.numCards = this._optionsService.cargarStorageOptions().numCards;
    this._kanaService.cargarDefault();
    console.log('this._kanaService.kanas', this._kanaService.kanas)
    this.kanas = this._kanaService.kanas;
  }

  kanaChanged(data:any) {
    this._kanaService.guardarStorage();
  }
  // add back when alpha.4 is out
  // navigate(item) {
  //   this.router.navigate(['/list', JSON.stringify(item)]);
  // }
}
