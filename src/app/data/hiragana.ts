export const Hiragana = [
  {
    "kana":"か",
    "fonema":"ka",
    "type":"hiragana",
    "helpDesc":"Recuerda al karateka",
    "active": true
  },
  {
    "kana":"け",
    "fonema":"ke",
    "type":"hiragana",
    "helpDesc":"Recuerda la Kapa y la Espada",
    "active": true
  },
  {
    "kana":"き",
    "fonema":"ki",
    "type":"hiragana",
    "helpDesc":"Recuerda la llave, la key /ki:/",
    "active": true
  },
  {
    "kana":"こ",
    "fonema":"ko",
    "type":"hiragana",
    "helpDesc":"La cobra de Cameron, la Kopia, una kopia encima del otro",
    "active": true
  },
  {
    "kana":"く",
    "fonema":"ku",
    "type":"hiragana",
    "helpDesc":"Kunio, KU-KU del comecocos o el patito haciendo KU-KU-KU",
    "active": true
  },
  {
    "kana":"た",
    "fonema":"ta",
    "type":"hiragana",
    "helpDesc":"El taburete, la Table, el ta escrito",
    "active": true
  },
  {
    "kana":"て",
    "fonema":"te",
    "type":"hiragana",
    "helpDesc":"El telefono en el lateral de la mesa",
    "active": true
  },
  {
    "kana":"ち",
    "fonema":"chi",
    "type":"hiragana",
    "helpDesc":"taiCHI",
    "active": true
  },
  {
    "kana":"と",
    "fonema":"to",
    "type":"hiragana",
    "helpDesc":"El tomate casi completo con su rabito",
    "active": true
  },
  {
    "kana":"つ",
    "fonema":"tsu",
    "type":"hiragana",
    "helpDesc":"La ola gigante, el tsunami",
    "active": true
  },
  {
    "kana":"な",
    "fonema":"Na",
    "type":"hiragana",
    "helpDesc":"Personal de la mariNA haciendo nudos",
    "active": false
  },
  {
    "kana":"ね",
    "fonema":"Ne",
    "type":"hiragana",
    "helpDesc":"Gato en japonés apoyado en una pata de la mesa",
    "active": false
  },
  {
    "kana":"に",
    "fonema":"Ni",
    "type":"hiragana",
    "helpDesc":"Bandera de NIppon, o el Ninja con capa que hace copias, ¿kakashi?",
    "active": false
  },
  {
    "kana":"の",
    "fonema":"No",
    "type":"hiragana",
    "helpDesc":"Señal prohibido, NO se puede",
    "active": true
  },
  {
    "kana":"ぬ",
    "fonema":"Nu",
    "type":"hiragana",
    "helpDesc":"Vamos a comer unos noodels, /NUdels/",
    "active": false
  },{
    "kana":"ま",
    "fonema":"Ma",
    "type":"hiragana",
    "helpDesc":"Mama está enfadada porque has hecho algo mal",
    "active": false
  },
  {
    "kana":"め",
    "fonema":"Me",
    "type":"hiragana",
    "helpDesc":"Me es ojo en japones, es un ojo cruzado por el medio",
    "active": false
  },
  {
    "kana":"み",
    "fonema":"Mi",
    "type":"hiragana",
    "helpDesc":"Es el numero 21, que es mi numero favorito, mi edad favorita, y mi tracklist guapi!",
    "active": false
  },
  {
    "kana":"も",
    "fonema":"Mo",
    "type":"hiragana",
    "helpDesc":"MOno, acostado en una rama, dejando la cola como si fuera un shireno",
    "active": false
  },
  {
    "kana":"む",
    "fonema":"Mu",
    "type":"hiragana",
    "helpDesc":"Una vaca, MUgiendo, diciendo MU!!!!!",
    "active": false
  },
  {
    "kana":"ら",
    "fonema":"Ra",
    "type":"hiragana",
    "helpDesc":"Un RAbbit de gran cabeza o un surfista, surfeando un tsunami, ya que la parte de abajo parece un ‘tsu’, y para surfear un tsunami, no puede ser nadie más que RAmbo",
    "active": false
  },
  {
    "kana":"れ",
    "fonema":"Re",
    "type":"hiragana",
    "helpDesc":"Un REptil REptando por un arbolito",
    "active": true
  },
  {
    "kana":"り",
    "fonema":"Ri",
    "type":"hiragana",
    "helpDesc":"Una copia ridicula de si mismo, ",
    "active": false
  },
  {
    "kana":"ろ",
    "fonema":"Ro",
    "type":"hiragana",
    "helpDesc":"ROma se construyo en 3 días",
    "active": true
  },
  {
    "kana":"る",
    "fonema":"Ru",
    "type":"hiragana",
    "helpDesc":"Roma se construyó en 3 días, y que hicieron tras eso, se pusieron a bailar una RUmba. RUta de montaña, RUmbo a un pico, donde seguro que hacemos mucha RUeda",
    "active": true
  },
  {
    "kana":"さ",
    "fonema":"Sa",
    "type":"hiragana",
    "helpDesc":"Un SAlmón o una SArdina o la cara de un SAmurai",
    "active": true
  },
  {
    "kana":"せ",
    "fonema":"Se",
    "type":"hiragana",
    "helpDesc":"Un samurai se hace el SEppuku o muchos Seven",
    "active": false
  },
  {
    "kana":"し",
    "fonema":"Shi",
    "type":"hiragana",
    "helpDesc":"Es el Shireno!!!",
    "active": true
  },
  {
    "kana":"そ",
    "fonema":"So",
    "type":"hiragana",
    "helpDesc":"El alma (SOul) jedi va a por los sith",
    "active": false
  },
  {
    "kana":"す",
    "fonema":"Su",
    "type":"hiragana",
    "helpDesc":"El panda subiendo el arbol o el luchador de sumo apunto de abrazar.",
    "active": true
  },
  {
    "kana":"や",
    "fonema":"Ya",
    "type":"hiragana",
    "helpDesc":"El YAk con sus cuernos",
    "active": true
  },
  {
    "kana":"よ",
    "fonema":"Yo",
    "type":"hiragana",
    "helpDesc":"Agarro el YO-YO gira y gira",
    "active": true
  },
  {
    "kana":"ゆ",
    "fonema":"Yu",
    "type":"hiragana",
    "helpDesc":"El pez pide aYUda y yo lo quiero de desaYUno",
    "active": true
  },
  {
    "kana":"は",
    "fonema":"Ha",
    "type":"hiragana",
    "helpDesc":"Es una H y una a pequeñita escrita",
    "active": false
  },
  {
    "kana":"へ",
    "fonema":"He",
    "type":"hiragana",
    "helpDesc":"La sílaba HE parece una montaña, podría recordarme un poco a la montaña Santa HElena, es un pico o la cabeza (HEad) de un niño",
    "active": false
  },
  {
    "kana":"ひ",
    "fonema":"Hi",
    "type":"hiragana",
    "helpDesc":"HIHIHIHI!!!, con una sonrisa estrambótica y anormal, una carcajada HIlarante",
    "active": true
  },
  {
    "kana":"ほ",
    "fonema":"Ho",
    "type":"hiragana",
    "helpDesc":"Una H y una o escrito de manera lateral subrayado",
    "active": true
  },
  {
    "kana":"ふ",
    "fonema":"Fu",
    "type":"hiragana",
    "helpDesc":"Un volcan, el monte Fuji, cuando tenía lava saliendo de sus entrañas",
    "active": false
  },
  {
    "kana":"あ",
    "fonema":"A",
    "type":"hiragana",
    "helpDesc":"Una persona en el Agua Atrapado por un remolino pidiendo Auxilio, pidiendo Ayuda, gritando AAAAAAA!",
    "active": false
  },
  {
    "kana":"え",
    "fonema":"E",
    "type":"hiragana",
    "helpDesc":"La vocal E es como un 3 invertido, subrayado por arriba, parecido a una EnE",
    "active": false
  },
  {
    "kana":"い",
    "fonema":"I",
    "type":"hiragana",
    "helpDesc":"La I hiragana es muy torpe, y siempre está tropezando, y siempre está tirando su puntito, pero de tanto tirarla, se ha estirado un poco, tiene cierta similitud con una i acentuada hacia el otro lado",
    "active": true
  },
  {
    "kana":"お",
    "fonema":"O",
    "type":"hiragana",
    "helpDesc":"La cara de una persona que está sufriendo un gran dolor",
    "active": false
  },
  {
    "kana":"う",
    "fonema":"U",
    "type":"hiragana",
    "helpDesc":"Buscamos la u oculta. Así es se encuentra ladeada, y lo de encima si lo pegamos es el rabito de la U",
    "active": false
  },
  {
    "kana":"わ",
    "fonema":"WA",
    "type":"hiragana",
    "helpDesc":"Me imagino a una persona con una gran barriga, y solo puede ser una embarazada, WAaw!!! que pedazo de barriga",
    "active": false
  },
  {
    "kana":"を",
    "fonema":"WO",
    "type":"hiragana",
    "helpDesc":"Vamos a la playa y vamos a tocar el agua con el pie, y está helada, y suelas un WOOOOO!!!",
    "active": false
  },
  {
    "kana":"ん",
    "fonema":"-N",
    "type":"hiragana",
    "helpDesc":"Es una N realmente con ganas de hacerse Notar, y estira su cabecita, pero como No puede sujetarse bien y se va hacia adelante",
    "active": true
  },
  {
    "kana":"が",
    "fonema":"GA",
    "type":"hiragana",
    "helpDesc":"KA => GA",
    "active": false
  },
  {
    "kana":"げ",
    "fonema":"GE",
    "type":"hiragana",
    "helpDesc":"KE => GE",
    "active": false
  },
  {
    "kana":"ぎ",
    "fonema":"GI",
    "type":"hiragana",
    "helpDesc":"KI => GI",
    "active": false
  },
  {
    "kana":"ご",
    "fonema":"GO",
    "type":"hiragana",
    "helpDesc":"KO => GO",
    "active": false
  },
  {
    "kana":"ぐ",
    "fonema":"GU",
    "type":"hiragana",
    "helpDesc":"KU => GU",
    "active": false
  },
  {
    "kana":"だ",
    "fonema":"DA",
    "type":"hiragana",
    "helpDesc":"TA => DA",
    "active": false
  },
  {
    "kana":"で",
    "fonema":"DE",
    "type":"hiragana",
    "helpDesc":"TE => DE",
    "active": false
  },
  {
    "kana":"ぢ",
    "fonema":"JI",
    "type":"hiragana",
    "helpDesc":"CHI => JI",
    "active": false
  },
  {
    "kana":"ど",
    "fonema":"DO",
    "type":"hiragana",
    "helpDesc":"TO => DO",
    "active": false
  },
  {
    "kana":"づ",
    "fonema":"ZU",
    "type":"hiragana",
    "helpDesc":"TSU => ZU",
    "active": false
  },
  {
    "kana":"ざ",
    "fonema":"ZA",
    "type":"hiragana",
    "helpDesc":"SA => ZA",
    "active": false
  },
  {
    "kana":"ぜ",
    "fonema":"ZE",
    "type":"hiragana",
    "helpDesc":"SE => ZE",
    "active": false
  },
  {
    "kana":"じ",
    "fonema":"JI",
    "type":"hiragana",
    "helpDesc":"SHI => JI",
    "active": false
  },
  {
    "kana":"ぞ",
    "fonema":"ZO",
    "type":"hiragana",
    "helpDesc":"SO => ZO",
    "active": false
  },
  {
    "kana":"ず",
    "fonema":"ZU",
    "type":"hiragana",
    "helpDesc":"SU => ZU",
    "active": false
  },
  {
    "kana":"ば",
    "fonema":"BA",
    "type":"hiragana",
    "helpDesc":"HA => BA",
    "active": false
  },
  {
    "kana":"べ",
    "fonema":"BE",
    "type":"hiragana",
    "helpDesc":"HE => BE",
    "active": false
  },
  {
    "kana":"び",
    "fonema":"BI",
    "type":"hiragana",
    "helpDesc":"HI => BI",
    "active": false
  },
  {
    "kana":"ぼ",
    "fonema":"BO",
    "type":"hiragana",
    "helpDesc":"HO => BO",
    "active": false
  },
  {
    "kana":"ぶ",
    "fonema":"BU",
    "type":"hiragana",
    "helpDesc":"FU => BU",
    "active": false
  },
  {
    "kana":"ぱ",
    "fonema":"PA",
    "type":"hiragana",
    "helpDesc":"HA => BA => PA",
    "active": false
  },
  {
    "kana":"ぺ",
    "fonema":"PE",
    "type":"hiragana",
    "helpDesc":"HE => BE => PE",
    "active": false
  },
  {
    "kana":"ぴ",
    "fonema":"PI",
    "type":"hiragana",
    "helpDesc":"HI => BI => PI",
    "active": false
  },
  {
    "kana":"ぽ",
    "fonema":"PO",
    "type":"hiragana",
    "helpDesc":"HO => BO => PO",
    "active": false
  },
  {
    "kana":"ぷ",
    "fonema":"PU",
    "type":"hiragana",
    "helpDesc":"FU => BU => PU",
    "active": false
  },
  {
    "kana":"じゃ",
    "fonema":"JA",
    "type":"hiragana",
    "helpDesc":"SHI + A => JA",
    "active": false
  },
  {
    "kana":"じょ",
    "fonema":"JO",
    "type":"hiragana",
    "helpDesc":"SHI + O => JO",
    "active": false
  },
  {
    "kana":"じゅ",
    "fonema":"JU",
    "type":"hiragana",
    "helpDesc":"SHI + U => JU",
    "active": false
  }
];