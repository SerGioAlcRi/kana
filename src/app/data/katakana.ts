export const Katakana = [
  {
    "kana":"カ",
    "fonema":"ka",
    "type":"katakana",
    "helpDesc":"Recuerda al karateka",
    "active": true
  },
  {
    "kana":"ケ",
    "fonema":"ke",
    "type":"katakana",
    "helpDesc":"",
    "active": true
  },
  {
    "kana":"キ",
    "fonema":"ki",
    "type":"katakana",
    "helpDesc":"Recuerda la llave, la key /ki:/",
    "active": true
  },
  {
    "kana":"コ",
    "fonema":"ko",
    "type":"katakana",
    "helpDesc":"",
    "active": true
  },
  {
    "kana":"ク",
    "fonema":"ku",
    "type":"katakana",
    "helpDesc":"Kuchara en el ramen",
    "active": true
  },
  {
    "kana":"タ",
    "fonema":"ta",
    "type":"katakana",
    "helpDesc":"El taburete, la Table, el ta escrito",
    "active": true
  },
  {
    "kana":"テ",
    "fonema":"te",
    "type":"katakana",
    "helpDesc":"El telefono en el lateral de la mesa",
    "active": true
  },
  {
    "kana":"チ",
    "fonema":"chi",
    "type":"katakana",
    "helpDesc":"Recordais el simbolo femenino",
    "active": true
  },
  {
    "kana":"ト",
    "fonema":"to",
    "type":"katakana",
    "helpDesc":"",
    "active": true
  },
  {
    "kana":"ツ",
    "fonema":"tsu",
    "type":"katakana",
    "helpDesc":"shiaTSU",
    "active": true
  },
  {
    "kana":"ナ",
    "fonema":"Na",
    "type":"katakana",
    "helpDesc":"",
    "active": false
  },
  {
    "kana":"ネ",
    "fonema":"Ne",
    "type":"katakana",
    "helpDesc":"",
    "active": false
  },
  {
    "kana":"ニ",
    "fonema":"Ni",
    "type":"katakana",
    "helpDesc":"Bandera de NIppon",
    "active": false
  },
  {
    "kana":"ノ",
    "fonema":"No",
    "type":"katakana",
    "helpDesc":"Señal prohibido, NO se puede",
    "active": true
  },
  {
    "kana":"ヌ",
    "fonema":"Nu",
    "type":"katakana",
    "helpDesc":"",
    "active": false
  },{
    "kana":"マ",
    "fonema":"Ma",
    "type":"katakana",
    "helpDesc":"",
    "active": false
  },
  {
    "kana":"メ",
    "fonema":"Me",
    "type":"katakana",
    "helpDesc":"es Medio No, ya que esta por la mitad",
    "active": false
  },
  {
    "kana":"ミ",
    "fonema":"Mi",
    "type":"katakana",
    "helpDesc":"Misiles",
    "active": false
  },
  {
    "kana":"モ",
    "fonema":"Mo",
    "type":"katakana",
    "helpDesc":"MOno, acostado en una rama, dejando la cola como si fuera un shireno",
    "active": false
  },
  {
    "kana":"ム",
    "fonema":"Mu",
    "type":"katakana",
    "helpDesc":"",
    "active": false
  },
  {
    "kana":"ラ",
    "fonema":"Ra",
    "type":"katakana",
    "helpDesc":"Los palillos encima de fuente de ramen que acabamos de comer",
    "active": false
  },
  {
    "kana":"レ",
    "fonema":"Re",
    "type":"katakana",
    "helpDesc":"Reflexion de la luz, rebote de la pelota",
    "active": true
  },
  {
    "kana":"リ",
    "fonema":"Ri",
    "type":"katakana",
    "helpDesc":"Una copia ridicula de si mismo, ",
    "active": false
  },
  {
    "kana":"ロ",
    "fonema":"Ro",
    "type":"katakana",
    "helpDesc":"Rosquilla cuadrada",
    "active": true
  },
  {
    "kana":"ル",
    "fonema":"Ru",
    "type":"katakana",
    "helpDesc":"",
    "active": true
  },
  {
    "kana":"サ",
    "fonema":"Sa",
    "type":"katakana",
    "helpDesc":"Rey salomon quiere partir al niño",
    "active": true
  },
  {
    "kana":"セ",
    "fonema":"Se",
    "type":"katakana",
    "helpDesc":"Un samurai se hace el SEppuku o 2 seven",
    "active": false
  },
  {
    "kana":"シ",
    "fonema":"Shi",
    "type":"katakana",
    "helpDesc":"SHIatsu",
    "active": true
  },
  {
    "kana":"ソ",
    "fonema":"So",
    "type":"katakana",
    "helpDesc":"",
    "active": false
  },
  {
    "kana":"ス",
    "fonema":"Su",
    "type":"katakana",
    "helpDesc":"Sujetamos tazon para que no se caiga",
    "active": true
  },
  {
    "kana":"ヤ",
    "fonema":"Ya",
    "type":"katakana",
    "helpDesc":"",
    "active": true
  },
  {
    "kana":"ヨ",
    "fonema":"Yo",
    "type":"katakana",
    "helpDesc":"",
    "active": true
  },
  {
    "kana":"ユ",
    "fonema":"Yu",
    "type":"katakana",
    "helpDesc":"",
    "active": true
  },
  {
    "kana":"ハ",
    "fonema":"Ha",
    "type":"katakana",
    "helpDesc":"",
    "active": false
  },
  {
    "kana":"ヘ",
    "fonema":"He",
    "type":"katakana",
    "helpDesc":"La sílaba HE parece una montaña, podría recordarme un poco a la montaña Santa HElena, es un pico o la cabeza (HEad) de un niño",
    "active": false
  },
  {
    "kana":"ヒ",
    "fonema":"Hi",
    "type":"katakana",
    "helpDesc":"",
    "active": true
  },
  {
    "kana":"ホ",
    "fonema":"Ho",
    "type":"katakana",
    "helpDesc":"",
    "active": true
  },
  {
    "kana":"フ",
    "fonema":"Fu",
    "type":"katakana",
    "helpDesc":"Una fuente de ramen",
    "active": false
  },
  {
    "kana":"ア",
    "fonema":"A",
    "type":"katakana",
    "helpDesc":"Es una A torcida",
    "active": false
  },
  {
    "kana":"エ",
    "fonema":"E",
    "type":"katakana",
    "helpDesc":"",
    "active": false
  },
  {
    "kana":"イ",
    "fonema":"I",
    "type":"katakana",
    "helpDesc":"",
    "active": true
  },
  {
    "kana":"オ",
    "fonema":"O",
    "type":"katakana",
    "helpDesc":"",
    "active": false
  },
  {
    "kana":"ウ",
    "fonema":"U",
    "type":"katakana",
    "helpDesc":"",
    "active": false
  },
  {
    "kana":"ワ",
    "fonema":"WA",
    "type":"katakana",
    "helpDesc":"",
    "active": false
  },
  {
    "kana":"ヲ",
    "fonema":"WO",
    "type":"katakana",
    "helpDesc":"",
    "active": false
  },
  {
    "kana":"ン",
    "fonema":"-N",
    "type":"katakana",
    "helpDesc":"",
    "active": true
  },
  {
    "kana":"ガ",
    "fonema":"GA",
    "type":"katakana",
    "helpDesc":"KA => GA",
    "active": false
  },
  {
    "kana":"ゲ",
    "fonema":"GE",
    "type":"katakana",
    "helpDesc":"KE => GE",
    "active": false
  },
  {
    "kana":"ギ",
    "fonema":"GI",
    "type":"katakana",
    "helpDesc":"KI => GI",
    "active": false
  },
  {
    "kana":"ゴ",
    "fonema":"GO",
    "type":"katakana",
    "helpDesc":"KO => GO",
    "active": false
  },
  {
    "kana":"グ",
    "fonema":"GU",
    "type":"katakana",
    "helpDesc":"KU => GU",
    "active": false
  },
  {
    "kana":"ダ",
    "fonema":"DA",
    "type":"katakana",
    "helpDesc":"TA => DA",
    "active": false
  },
  {
    "kana":"デ",
    "fonema":"DE",
    "type":"katakana",
    "helpDesc":"TE => DE",
    "active": false
  },
  {
    "kana":"ジ",
    "fonema":"JI",
    "type":"katakana",
    "helpDesc":"CHI => JI",
    "active": false
  },
  {
    "kana":"ド",
    "fonema":"DO",
    "type":"katakana",
    "helpDesc":"TO => DO",
    "active": false
  },
  {
    "kana":"ヅ",
    "fonema":"ZU",
    "type":"katakana",
    "helpDesc":"TSU => ZU",
    "active": false
  },
  {
    "kana":"ザ",
    "fonema":"ZA",
    "type":"katakana",
    "helpDesc":"SA => ZA",
    "active": false
  },
  {
    "kana":"ゼ",
    "fonema":"ZE",
    "type":"katakana",
    "helpDesc":"SE => ZE",
    "active": false
  },
  {
    "kana":"ジ",
    "fonema":"JI",
    "type":"katakana",
    "helpDesc":"SHI => JI",
    "active": false
  },
  {
    "kana":"ゾ",
    "fonema":"ZO",
    "type":"katakana",
    "helpDesc":"SO => ZO",
    "active": false
  },
  {
    "kana":"ズ",
    "fonema":"ZU",
    "type":"katakana",
    "helpDesc":"SU => ZU",
    "active": false
  },
  {
    "kana":"バ",
    "fonema":"BA",
    "type":"katakana",
    "helpDesc":"HA => BA",
    "active": false
  },
  {
    "kana":"べ",
    "fonema":"BE",
    "type":"katakana",
    "helpDesc":"HE => BE",
    "active": false
  },
  {
    "kana":"ビ",
    "fonema":"BI",
    "type":"katakana",
    "helpDesc":"HI => BI",
    "active": false
  },
  {
    "kana":"ボ",
    "fonema":"BO",
    "type":"katakana",
    "helpDesc":"HO => BO",
    "active": false
  },
  {
    "kana":"ブ",
    "fonema":"BU",
    "type":"katakana",
    "helpDesc":"FU => BU",
    "active": false
  },
  {
    "kana":"パ",
    "fonema":"PA",
    "type":"katakana",
    "helpDesc":"HA => BA => PA",
    "active": false
  },
  {
    "kana":"ぺ",
    "fonema":"PE",
    "type":"katakana",
    "helpDesc":"HE => BE => PE",
    "active": false
  },
  {
    "kana":"ピ",
    "fonema":"PI",
    "type":"katakana",
    "helpDesc":"HI => BI => PI",
    "active": false
  },
  {
    "kana":"ポ",
    "fonema":"PO",
    "type":"katakana",
    "helpDesc":"HO => BO => PO",
    "active": false
  },
  {
    "kana":"プ",
    "fonema":"PU",
    "type":"katakana",
    "helpDesc":"FU => BU => PU",
    "active": false
  },
  {
    "kana":"ジャ",
    "fonema":"JA",
    "type":"katakana",
    "helpDesc":"SHI + A => JA",
    "active": false
  },
  {
    "kana":"ジョ",
    "fonema":"JO",
    "type":"katakana",
    "helpDesc":"SHI + O => JO",
    "active": false
  },
  {
    "kana":"ジュ",
    "fonema":"JU",
    "type":"katakana",
    "helpDesc":"SHI + U => JU",
    "active": false
  }
];